/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jinan_alhajjaj
 */
public class Scholarly {
    public String pageId;
    public String title;
    public String revId;    
    public String  timestamp;
    public String type;
    public String id;

    public Scholarly() {
        this.pageId = "";
        this.title = "";
        this.revId = "";
        this.timestamp = "";
        this.type = "";
        this.id = "";

    }

    public String getPageId() {
        return pageId;
    }

    public void setPageId(String pageId) {
        this.pageId = pageId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRevId() {
        return revId;
    }

    public void setRevId(String revId) {
        this.revId = revId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public Scholarly( String pageId,String title,String revId,String  timestamp,String type,String id) {
        this.pageId =pageId ;
        this.title = title;
        this.revId = revId;
        this.timestamp =timestamp;
        this.type = type;
        this.id = id;
    } 
}

    

