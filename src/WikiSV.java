import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
public class WikiSV {
    private static String Wikifilename = "C:\\Users\\Jinan_alhajjaj\\Desktop\\level 9\\senior year\\wikifile.xml";
    private static String scholarfilename = "C:\\Users\\Jinan_alhajjaj\\Desktop\\level 9\\doi_isbn_pubmed_and_arxiv.enwiki-20150602.tsv";
    private static LinkedList<String> Scholarlist = new LinkedList<String>();
    private static String titleactual = null;
    public static void main(String[] args) {
        boolean title = false;
        boolean isRequest = false;
        String text = null;
        Scanner input=new Scanner(System.in);
        System.out.print("please,Enter the subject : ");
        String requested_subject=input.next();
        LinkedList<String> Book = new LinkedList<String>();
        LinkedList<String> Journal = new LinkedList<String>();
        LinkedList<String> Thesis = new LinkedList<String>();
        LinkedList<String> Web = new LinkedList<String>();
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader eventReader = factory.createXMLEventReader(new FileReader(Wikifilename));
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                switch (event.getEventType()) {
                    case XMLStreamConstants.START_ELEMENT:
                        StartElement startElement = event.asStartElement();
                        String qName = startElement.getName().getLocalPart();
                        if (qName.equalsIgnoreCase("page")) {
                            isRequest = true;
                        }
                        if (qName.equalsIgnoreCase("title") && isRequest)
                        {
                            titleactual = eventReader.getElementText();
                            if(titleactual.contains(requested_subject))
                            {
                                System.out.println("title: " + titleactual);
                                title = true;
                            }  
                            else
                                break;
                             
                        }
                        if (qName.equalsIgnoreCase("text") && isRequest && title) {

                            text = eventReader.getElementText();
                            int pos = text.indexOf("{{cite", 0);
                            if (pos < 0) {
                                System.out.println("No Reference found ");
                                break;
                            }
                            scholararticle();
                            int end = 0;
                            while ((end = text.indexOf("}}", pos)) >= 0) {
                                if (pos < 0) {
                                    break;
                                }
                                String cite = text.substring(pos, end).toLowerCase();
                                String refType = "";
                                String formatedRef = "";
                                if (cite.contains("|")) {
                                    refType = cite.substring(cite.indexOf("{{cite") + 6, cite.indexOf("|")).toLowerCase();
                                } else {
                                    refType = cite.substring(cite.indexOf("{{cite") + 6).toLowerCase();
                                }
                                if (refType.contains("book")) {
                                    formatedRef = formating(cite, end);
                                    Book.add(formatedRef + "\n");
                                }
                                if (refType.contains("web")) {
                                    formatedRef = formating(cite, end);
                                    Web.add(formatedRef + "\n");
                                }
                                if (refType.contains("thesis")) {
                                    formatedRef = formating(cite, end);
                                    Thesis.add(formatedRef + "\n");
                                }
                                if (refType.contains("journal")) {
                                    formatedRef = formating(cite, end);
                                    Journal.add(formatedRef + "\n");
                                }
                                pos = end + 1;
                                pos = text.indexOf("{{cite", pos);
                            }
                            Scholarlist.clear();
                        }
                        break;                        
                    case XMLStreamConstants.END_ELEMENT:
                        EndElement endElement = event.asEndElement();
                        if (endElement.getName().getLocalPart().equalsIgnoreCase("page") && isRequest && title) {
                            System.out.println();
                            isRequest = false;
                            title = false;
                        }
                        break;
                }
            }
            System.out.println("Thesis:");
            for (String ref : Thesis) {
                System.out.println(ref);
            }
            System.out.println("Web:");
            for (String ref : Web) {
                System.out.println(ref);
            }
            System.out.println("Books:");
            for (String ref : Book) {

                System.out.println(ref);
            }
            System.out.println("Journal:");
            for (String ref : Journal) {
                System.out.println(ref);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    public static void scholararticle() {
        try (BufferedReader file = new BufferedReader(new FileReader(scholarfilename))) {
            String record;
            String scholartype;
            Scholarly scholarly = new Scholarly();
            while ((record = file.readLine()) != null) {
                String[] fields = record.split("\t");
                if (fields.length == 6) {
                    scholarly.pageId = fields[0];
                    scholarly.title = fields[1];
                    if (!(scholarly.title.equalsIgnoreCase(titleactual))) {
                        continue;
                    }
                    scholarly.revId = fields[2];
                    scholarly.timestamp = fields[3];
                    scholarly.type = fields[4];
                    scholarly.id = fields[5];
                    scholartype = scholarly.type + "=" + scholarly.id;
                    Scholarlist.add(scholartype);
                } else {
                    System.out.println("Error: Invalid File!");

                }
            }
            file.close();
        } catch (Exception exc) {
            System.out.println("Exception:" + exc);
        }
    }

    public static String formating(String cite, int end) {
        cite = cite.substring(cite.indexOf("|") + 1);
        cite = cite.concat("|");
        String formatedCite = null;
        String title = null;
        String author = null;
        String first = null;
        String last = null;
        String date = null;
        String url = null;
        String location = null;
        String publisher = null;
        String isbn = null;
        String accessdate = null;
        String journal = null;
        String quote = null;
        String website = null;
        String docket = null;
        String chapter = null;
        String type = null;
        String doi = null;
        String pmid = null;
        String pmc = null;
        String arxiv = null;
        String oclc = null;
        String page = null;
        String volume = null;
        String trust = null;
        if (cite.contains("title")) {
            String copy = cite;
            int start = copy.indexOf("title");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                title = copy.substring(copy.indexOf("title"), copy.indexOf("|", copy.indexOf("title")));
                if (!(title.contains("="))) {
                    title = " ";
                    copy = copy.substring(e);
                    if (copy.contains("title")) {
                        start = copy.indexOf("title");
                    } else {
                        break;
                    }
                } else {
                    String check = title.substring(title.indexOf("title"), title.indexOf("="));
                    if (check.equals("title") || check.equals("title ")) {
                        break;
                    } else {
                        title = " ";
                        copy = copy.substring(e);
                        if (copy.contains("title")) {
                            start = copy.indexOf("title");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("first")) {
            String copy = cite;
            int start = copy.indexOf("first");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                first = copy.substring(copy.indexOf("first"), copy.indexOf("|", copy.indexOf("first")));
                if (!(first.contains("="))) {
                    first = " ";
                    copy = copy.substring(e);
                    if (copy.contains("first")) {
                        start = copy.indexOf("first");
                    } else {
                        break;
                    }
                } else {
                    String check = first.substring(first.indexOf("first"), first.indexOf("="));
                    if (check.equals("first") || check.equals("first")) {
                        first = first.substring(first.indexOf("=") + 1);
                        break;
                    } else {
                        first = " ";
                        copy = copy.substring(e);
                        if (copy.contains("first")) {
                            start = copy.indexOf("first");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("last")) {
            String copy = cite;
            int start = copy.indexOf("last");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                last = copy.substring(copy.indexOf("last"), copy.indexOf("|", copy.indexOf("last")));
                if (!(last.contains("="))) {
                    last = " ";
                    copy = copy.substring(e);
                    if (copy.contains("last")) {
                        start = copy.indexOf("last");
                    } else {
                        break;
                    }
                } else {
                    String check = last.substring(last.indexOf("last"), last.indexOf("="));
                    if (check.equals("last") || check.equals("last")) {
                        last = last.substring(last.indexOf("=") + 1);
                        break;
                    } else {
                        last = " ";
                        copy = copy.substring(e);
                        if (copy.contains("last")) {
                            start = copy.indexOf("last");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("access-date")) {
            String copy = cite;
            int start = copy.indexOf("access-date");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                accessdate = copy.substring(copy.indexOf("access-date"), copy.indexOf("|", copy.indexOf("access-date")));
                if (!(accessdate.contains("="))) {
                    accessdate = " ";
                    copy = copy.substring(e);
                    if (copy.contains("access-date")) {
                        start = copy.indexOf("access-date");
                    } else {
                        break;
                    }
                } else {
                    String check = accessdate.substring(accessdate.indexOf("access-date"), accessdate.indexOf("="));
                    if (check.equals("access-date") || check.equals("access-date ")) {
                        break;
                    } else {
                        accessdate = " ";
                        copy = copy.substring(e);
                        if (copy.contains("access-date")) {
                            start = copy.indexOf("access-date");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("date")) {
            String copy = cite;
            int start = copy.indexOf("date");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                date = copy.substring(copy.indexOf("date"), copy.indexOf("|", copy.indexOf("date")));
                if (!(date.contains("="))) {
                    date = " ";
                    copy = copy.substring(e);
                    if (copy.contains("date")) {
                        start = copy.indexOf("date");
                    } else {
                        break;
                    }
                } else {
                    String check = date.substring(date.indexOf("date"), date.indexOf("="));
                    if (check.equals("date") || check.equals("date ")) {
                        break;
                    } else {
                        date = " ";
                        copy = copy.substring(e);
                        if (copy.contains("date")) {
                            start = copy.indexOf("date");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("url")) {
            String copy = cite;
            int start = copy.indexOf("url");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                url = copy.substring(copy.indexOf("url"), copy.indexOf("|", copy.indexOf("url")));
                if (!(url.contains("="))) {
                    url = " ";
                    copy = copy.substring(e);
                    if (copy.contains("url")) {
                        start = copy.indexOf("url");
                    } else {
                        break;
                    }
                } else {
                    String check = url.substring(url.indexOf("url"), url.indexOf("="));
                    if (check.equals("url") || check.equals("url ")) {
                        break;
                    } else {
                        url = " ";
                        copy = copy.substring(e);
                        if (copy.contains("url")) {
                            start = copy.indexOf("url");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("location")) {
            String copy = cite;
            int start = copy.indexOf("location");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                location = copy.substring(copy.indexOf("location"), copy.indexOf("|", copy.indexOf("location")));
                if (!(location.contains("="))) {
                    location = " ";
                    copy = copy.substring(e);
                    if (copy.contains("location")) {
                        start = copy.indexOf("location");
                    } else {
                        break;
                    }
                } else {
                    String check = location.substring(location.indexOf("location"), location.indexOf("="));
                    if (check.equals("location") || check.equals("location ")) {
                        break;
                    } else {
                        location = " ";
                        copy = copy.substring(e);
                        if (copy.contains("location")) {
                            start = copy.indexOf("location");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("publisher")) {
            String copy = cite;
            int start = copy.indexOf("publisher");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                publisher = copy.substring(copy.indexOf("publisher"), copy.indexOf("|", copy.indexOf("publisher")));
                if (!(publisher.contains("="))) {
                    publisher = " ";
                    copy = copy.substring(e);
                    if (copy.contains("publisher")) {
                        start = copy.indexOf("publisher");
                    } else {
                        break;
                    }
                } else {
                    String check = publisher.substring(publisher.indexOf("publisher"), publisher.indexOf("="));
                    if (check.equals("publisher") || check.equals("publisher ")) {
                        break;
                    } else {
                        publisher = " ";
                        copy = copy.substring(e);
                        if (copy.contains("publisher")) {
                            start = copy.indexOf("publisher");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("isbn")) {
            String copy = cite;
            int start = copy.indexOf("isbn");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                isbn = copy.substring(copy.indexOf("isbn"), copy.indexOf("|", copy.indexOf("isbn")));
                if (!(isbn.contains("="))) {
                    isbn = " ";
                    copy = copy.substring(e);
                    if (copy.contains("isbn")) {
                        start = copy.indexOf("isbn");
                    } else {
                        break;
                    }
                } else {
                    String check = isbn.substring(isbn.indexOf("isbn"), isbn.indexOf("="));
                    if (check.equals("isbn") || check.equals("isbn ")) {

                        break;
                    } else {
                        isbn = " ";
                        copy = copy.substring(e);
                        if (copy.contains("isbn")) {
                            start = copy.indexOf("isbn");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("journal")) {
            String copy = cite;
            int start = copy.indexOf("journal");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                journal = copy.substring(copy.indexOf("journal"), copy.indexOf("|", copy.indexOf("journal")));
                if (!(journal.contains("="))) {
                    journal = " ";
                    copy = copy.substring(e);
                    if (copy.contains("journal")) {
                        start = copy.indexOf("journal");
                    } else {
                        break;
                    }
                } else {
                    String check = journal.substring(journal.indexOf("journal"), journal.indexOf("="));
                    if (check.equals("journal") || check.equals("journal ")) {
                        break;
                    } else {
                        journal = " ";
                        copy = copy.substring(e);
                        if (copy.contains("journal")) {
                            start = copy.indexOf("journal");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("quote")) {
            String copy = cite;
            int start = copy.indexOf("quote");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                quote = copy.substring(copy.indexOf("quote"), copy.indexOf("|", copy.indexOf("quote")));
                if (!(quote.contains("="))) {
                    quote = " ";
                    copy = copy.substring(e);
                    if (copy.contains("quote")) {
                        start = copy.indexOf("quote");
                    } else {
                        break;
                    }
                } else {
                    String check = quote.substring(quote.indexOf("quote"), quote.indexOf("="));
                    if (check.equals("quote") || check.equals("quote")) {
                        break;
                    } else {
                        quote = " ";
                        copy = copy.substring(e);
                        if (copy.contains("quote")) {
                            start = copy.indexOf("quote");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("website")) {
            String copy = cite;
            int start = copy.indexOf("website");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                website = copy.substring(copy.indexOf("website"), copy.indexOf("|", copy.indexOf("website")));
                if (!(website.contains("="))) {
                    website = " ";
                    copy = copy.substring(e);
                    if (copy.contains("website")) {
                        start = copy.indexOf("website");
                    } else {
                        break;
                    }
                } else {
                    String check = website.substring(website.indexOf("website"), website.indexOf("="));
                    if (check.equals("website") || check.equals("website")) {
                        break;
                    } else {
                        website = " ";
                        copy = copy.substring(e);
                        if (copy.contains("website")) {
                            start = copy.indexOf("website");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("docket")) {
            String copy = cite;
            int start = copy.indexOf("docket");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                docket = copy.substring(copy.indexOf("docket"), copy.indexOf("|", copy.indexOf("docket")));
                if (!(docket.contains("="))) {
                    docket = " ";
                    copy = copy.substring(e);
                    if (copy.contains("docket")) {
                        start = copy.indexOf("docket");
                    } else {
                        break;
                    }
                } else {
                    String check = docket.substring(docket.indexOf("docket"), docket.indexOf("="));
                    if (check.equals("docket") || check.equals("docket")) {
                        break;
                    } else {
                        docket = " ";
                        copy = copy.substring(e);
                        if (copy.contains("docket")) {
                            start = copy.indexOf("docket");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("chapter")) {
            String copy = cite;
            int start = copy.indexOf("chapter");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                chapter = copy.substring(copy.indexOf("chapter"), copy.indexOf("|", copy.indexOf("chapter")));
                if (!(chapter.contains("="))) {
                    chapter = " ";
                    copy = copy.substring(e);
                    if (copy.contains("chapter")) {
                        start = copy.indexOf("chapter");
                    } else {
                        break;
                    }
                } else {
                    String check = chapter.substring(chapter.indexOf("chapter"), chapter.indexOf("="));
                    if (check.equals("chapter") || check.equals("chapter")) {
                        break;
                    } else {
                        chapter = " ";
                        copy = copy.substring(e);
                        if (copy.contains("chapter")) {
                            start = copy.indexOf("chapter");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("type")) {
            String copy = cite;
            int start = copy.indexOf("type");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                type = copy.substring(copy.indexOf("type"), copy.indexOf("|", copy.indexOf("type")));
                if (!(type.contains("="))) {
                    type = " ";
                    copy = copy.substring(e);
                    if (copy.contains("type")) {
                        start = copy.indexOf("type");
                    } else {
                        break;
                    }
                } else {
                    String check = type.substring(type.indexOf("type"), type.indexOf("="));
                    if (check.equals("type") || check.equals("type")) {
                        break;
                    } else {
                        type = " ";
                        copy = copy.substring(e);
                        if (copy.contains("type")) {
                            start = copy.indexOf("type");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("doi")) {
            String copy = cite;
            int start = copy.indexOf("doi");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                doi = copy.substring(copy.indexOf("doi"), copy.indexOf("|", copy.indexOf("doi")));
                if (!(doi.contains("="))) {
                    doi = " ";
                    copy = copy.substring(e);
                    if (copy.contains("doi")) {
                        start = copy.indexOf("doi");
                    } else {
                        break;
                    }
                } else {
                    String check = doi.substring(doi.indexOf("doi"), doi.indexOf("="));
                    if (check.equals("doi") || check.equals("doi")) {
                        doi = doi.substring(doi.indexOf("=") + 1);
                        break;
                    } else {
                        doi = " ";
                        copy = copy.substring(e);
                        if (copy.contains("doi")) {
                            start = copy.indexOf("doi");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("oclc")) {
            String copy = cite;
            int start = copy.indexOf("oclc");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                oclc = copy.substring(copy.indexOf("oclc"), copy.indexOf("|", copy.indexOf("oclc")));
                if (!(oclc.contains("="))) {
                    oclc = " ";
                    copy = copy.substring(e);
                    if (copy.contains("oclc")) {
                        start = copy.indexOf("oclc");
                    } else {
                        break;
                    }
                } else {
                    String check = oclc.substring(oclc.indexOf("oclc"), oclc.indexOf("="));
                    if (check.equals("oclc") || check.equals("oclc")) {
                        oclc = oclc.substring(oclc.indexOf("=") + 1);
                        break;
                    } else {
                        oclc = " ";
                        copy = copy.substring(e);
                        if (copy.contains("oclc")) {
                            start = copy.indexOf("oclc");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("page")) {
            String copy = cite;
            int start = copy.indexOf("page");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                page = copy.substring(copy.indexOf("page"), copy.indexOf("|", copy.indexOf("page")));
                if (!(page.contains("="))) {
                    page = " ";
                    copy = copy.substring(e);
                    if (copy.contains("page")) {
                        start = copy.indexOf("page");
                    } else {
                        break;
                    }
                } else {
                    String check = page.substring(page.indexOf("page"), page.indexOf("="));
                    if (check.equals("page") || check.equals("page")) {
                        break;
                    } else {
                        page = " ";
                        copy = copy.substring(e);
                        if (copy.contains("page")) {
                            start = copy.indexOf("page");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("pages")) {
            String copy = cite;
            int start = copy.indexOf("pages");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                page = copy.substring(copy.indexOf("pages"), copy.indexOf("|", copy.indexOf("pages")));
                if (!(page.contains("="))) {
                    page = " ";
                    copy = copy.substring(e);
                    if (copy.contains("pages")) {
                        start = copy.indexOf("pages");
                    } else {
                        break;
                    }
                } else {
                    String check = page.substring(page.indexOf("pages"), page.indexOf("="));
                    if (check.equals("pages") || check.equals("pages")) {
                        break;
                    } else {
                        page = " ";
                        copy = copy.substring(e);
                        if (copy.contains("pages")) {
                            start = copy.indexOf("pages");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("volume")) {
            String copy = cite;
            int start = copy.indexOf("volume");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                volume = copy.substring(copy.indexOf("volume"), copy.indexOf("|", copy.indexOf("volume")));
                if (!(volume.contains("="))) {
                    volume = " ";
                    copy = copy.substring(e);
                    if (copy.contains("volume")) {
                        start = copy.indexOf("volume");
                    } else {
                        break;
                    }
                } else {
                    String check = volume.substring(volume.indexOf("volume"), volume.indexOf("="));
                    if (check.equals("volume") || check.equals("volume")) {
                        break;
                    } else {
                        volume = " ";
                        copy = copy.substring(e);
                        if (copy.contains("volume")) {
                            start = copy.indexOf("volume");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("pmid")) {
            String copy = cite;
            int start = copy.indexOf("pmid");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                pmid = copy.substring(copy.indexOf("pmid"), copy.indexOf("|", copy.indexOf("pmid")));
                if (!(pmid.contains("="))) {
                    pmid = " ";
                    copy = copy.substring(e);
                    if (copy.contains("pmid")) {
                        start = copy.indexOf("pmid");
                    } else {
                        break;
                    }
                } else {
                    String check = pmid.substring(pmid.indexOf("pmid"), pmid.indexOf("="));
                    if (check.equals("pmid") || check.equals("pmid")) {
                        break;
                    } else {
                        pmid = " ";
                        copy = copy.substring(e);
                        if (copy.contains("pmid")) {
                            start = copy.indexOf("pmid");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("pmc")) {
            String copy = cite;
            int start = copy.indexOf("pmc");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                pmc = copy.substring(copy.indexOf("pmc"), copy.indexOf("|", copy.indexOf("pmc")));
                if (!(pmc.contains("="))) {
                    pmc = " ";
                    copy = copy.substring(e);
                    if (copy.contains("pmc")) {
                        start = copy.indexOf("pmc");
                    } else {
                        break;
                    }
                } else {
                    String check = pmc.substring(pmc.indexOf("pmc"), pmc.indexOf("="));
                    if (check.equals("pmc") || check.equals("pmc")) {
                        break;
                    } else {
                        pmc = " ";
                        copy = copy.substring(e);
                        if (copy.contains("pmc")) {
                            start = copy.indexOf("pmc");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        if (cite.contains("arxiv")) {
            String copy = cite;
            int start = copy.indexOf("arxiv");
            int e = 0;
            while ((e = copy.indexOf("|", start)) >= 0) {
                arxiv = copy.substring(copy.indexOf("arxiv"), copy.indexOf("|", copy.indexOf("arxiv")));
                if (!(arxiv.contains("="))) {
                    arxiv = " ";
                    copy = copy.substring(e);
                    if (copy.contains("arxiv")) {
                        start = copy.indexOf("arxiv");
                    } else {
                        break;
                    }
                } else {
                    String check = arxiv.substring(arxiv.indexOf("arxiv"), arxiv.indexOf("="));
                    if (check.equals("arxiv") || check.equals("arxiv")) {
                        break;
                    } else {
                        arxiv = " ";
                        copy = copy.substring(e);
                        if (copy.contains("arxiv")) {
                            start = copy.indexOf("arxiv");
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        for (String S : Scholarlist) {
            if (S.equalsIgnoreCase(arxiv) || S.equalsIgnoreCase(doi) || S.equalsIgnoreCase(isbn) || S.equalsIgnoreCase(pmc) || S.equalsIgnoreCase(pmid)) {
                trust = "Scholar Reference";
            }
        }
        author = first + " " + last;
        formatedCite = trust + "\n" + title + "\n" + "Author = " + author + "\n" + date + "\n" + url + "\n" + location + "\n" + publisher + "\n" + isbn + "\n" + accessdate + "\n" + journal + "\n" + quote + "\n" + website
                + "\n" + docket + "\n" + chapter + "\n" + type + "\n" + "Digital Object Identifier (DOI)= " + doi + "\n" + "WorldCat's Online Computer Library Center(OCLC)= " + oclc + "\n" + page + "\n" + volume;
        return formatedCite;
    }
}
